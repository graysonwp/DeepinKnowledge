当一个 table 刚创建的时候，HBase 默认只会分配一个 region 给这个 table，也就是说这个时候所有的读写请求都会访问到同一个 RegionServer 的同一个 Region 中，这个时候就达不到负载均衡的效果了，集群中的其他 RegionServer 可能处于比较空闲的状态，解决这个问题就可以使用预分区，在创建 table 的时候就配置好，生成多个 Region。


预分区具有如下好处：

* 增加数据读写效率；
* 负载均衡，防止数据倾斜；
* 方便集群容灾调度 Region；
* 优化 Map 数量，因为 HBase 中 MapReduce 合并时 Map 个数取决于 Region 个数。

预分区的原理：每一个 Region 维护着 startRow 和 endRow，如果加入的数据符合某个 region 维护的 rowKey 范围，则该数据交给这个 Region 维护。

#### 4.5.9 HBase 表的热点问题及其解决方法

热点问题是指当大量的 client 访问 HBase 集群的一个或少数几个节点，造成部分 RegionServer 的读/写请求过多、负载过大，而其它 RegionServer 负载却很小；

热点问题有如下解决方法：

* **预分区：** 预分区的目的是让表的数据可以均衡的分散在集群中，而不是默认只有一个 Region 分布在集群的一个节点上；
* **哈希：** 哈希可以使负载分散到整个集群，例如：`rowKey = MD5(username).subString(0, 10) + 时间戳 `；
* **反转：** 翻转固定长度或者数字格式的 rowKey 可以使得 rowKey 中经常改变的部分（最没有意义的部分）放在前面，例如：

```txt
电信公司：
移动-----------> 136xxxx9301  ----->1039xxxx631
```

