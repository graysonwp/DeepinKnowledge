- **Sort By:** 分区内有序。
- **Order By:** 全局排序，只有一个 Reducer。
- **Distribute By:**  类似 MR 中的 Partition，进行分区，将 map 端查询的结果中 hash 值相同的结果分发到对应的 reduce 文件夹中，结合 Sort By 使用。
- **Cluster By:** 当 Distribute By 和 Sort By 字段相同时，可以使用 Cluster By 方式。Cluster By 除了具有 Distribute By 的功能外还兼具 Sort By 的功能。但是排序只能是升序排序，不能指定排序规则为 ASC 或者 DESC。

