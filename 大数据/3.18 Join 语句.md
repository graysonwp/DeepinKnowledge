![](/media/202105//1621916801.085754.png)


* **内连接：** 只有进行连接的两个表中都存在与连接条件相匹配的数据才会被保留下来，Hive 中的 join 默认是内连接。
  * ```sql
    select * from teacher t inner join course c  on t.t_id = c.t_id;
    ```
* **左外连接：** join 操作符左边表中符合 where 子句的所有记录将会被返回，右边表的指定字段没有符合条件的值的话，那么就使用 null 值代替，右外连接和左外连接类似。
  * ```sql
    select * from teacher t left outer join course c on t.t_id = c.t_id;
    ```
* **满外连接：** 将会返回表中符合 where 条件的所有记录，如果任一表的指定字段没有符合条件的值的话，那么就使用 null 值代替。
  * ```sql
    select * from teacher t full outer join course c on t.t_id = c.t_id;
    ```
