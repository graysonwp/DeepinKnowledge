## 1 安装《Find sci paper》插件

【附件】[find-sci-paper-0.4_0.zip](https://notebook.grayson.top/media/attachment/2021/06/find-sci-paper-0.4_0.zip)

将上述插件下载并解压后，拖到谷歌浏览器里面进行安装即可。

## 2 搜索一篇 SCI 文章

![](./media/202105/2021-05-27_202026.png)

## 3 打开一篇 SCI 网址

![](./media/202105/2021-05-27_202153.png)

## 4 点击右上角的 Find sci paper 图标，然后会跳转到 Sci-Hub 页面

![](./media/202105/2021-05-27_202339.png)

![](./media/202105/2021-05-27_202441.png)

## 5 点击下载即可

![](./media/202105/2021-05-27_202619.png)

