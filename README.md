<p align='center'>
<a href="https://github.com/graysonwp/DeepinKnowledge/blob/main/LICENSE"><img alt="GitHub" src="https://img.shields.io/github/license/graysonwp/DeepinKnowledge?label=License"></a>
<img src="https://img.shields.io/badge/build-passing-brightgreen.svg">
<img src="https://img.shields.io/badge/platform-%20iOS | Android | Mac | Web%20-ff69b4.svg">
<img src="https://img.shields.io/badge/language-Java-orange.svg">
<img src="https://img.shields.io/badge/made%20with-=1-blue.svg">
<a href="https://github.com/graysonwp/DeepinKnowledge/pulls"><img src="https://img.shields.io/badge/PR-Welcome-brightgreen.svg"></a>
<img src="https://img.shields.io/github/stars/graysonwp/DeepinKnowledge?style=social">
<img src="https://img.shields.io/github/forks/graysonwp/DeepinKnowledge?style=social">
<a href="https://github.com/graysonwp/DeepinKnowledge"><img src="https://visitor-badge.laobi.icu/badge?page_id=graysonwp.ComputerCookbook-SchoolRecruitment"></a>
<a href="https://github.com/graysonwp/DeepinKnowledge/releases"><img src="https://img.shields.io/github/v/release/graysonwp/DeepinKnowledge"></a>
<a href="https://github.com/graysonwp/DeepinKnowledge"><img src="https://img.shields.io/github/repo-size/graysonwp/DeepinKnowledge"></a>
</p>

<p align='center'>
<a href="https://www.grayson.top"><img src="https://img.shields.io/badge/Blog-Grayson-80d4f9.svg?style=flat"></a>
<a href="https://unsplash.com/@graysonwp"><img src="https://img.shields.io/badge/Unsplash-Grayson-success.svg"></a>
 <a href="https://www.zhihu.com/people/wei-peng-36-39"><img src="https://img.shields.io/badge/%E7%9F%A5%E4%B9%8E-@Grayson-fd6f32.svg?style=flat&colorA=0083ea"></a>
</p>

<br>

<p align='center'>
  <img src="media/DeepinKnowledge-cover-A4.png">
</p>

<br>

## ⭐️ 为什么要建这个仓库

天道酬勤，厚积薄发。知识是不断积累的，有时我们可能经过一番努力，把一个知识点给弄明白了，但是中间可能很长时间不用，就慢慢淡忘了，当我们在某一天在需要用到这个知识时，可能有需要重新查资料去学习，这无疑是在浪费时间。如果我们把弄明白的知识按照自己的思路梳理出来，并将其整理成文档的形式，这样当我们再次需要用到这个知识时，就可以直接查看相应的文档，此时如果有新的想法或者当时理解不正确的地方，我们可以对这个知识进行更新，这样循环迭代，我们对这个只是的理解就会越来越深刻。一个知识是这样，如果我们不断积累，那么就会逐渐形成自己的体系，把这个分享出来，大家共同去维护这个知识库，当别人有需要时，直接查看，这样就省去了在此整理的时间，如果其他人又好的想法，不断地去更新他，集思广益，这个知识就会变得越来越丰富，坚持下来的话，这将会是一件很有意义的事。我会不断更新这个仓库，也希望大家如果有什么想法的话，欢迎在`issues`中提出来，或者通过`pull`来提交，让我们大家一起共同维护这个知识库，完成这件有意义的事。

## :hamster: ​一些说明

:snail: 这个项目的[计算机基础](#user-content-snake-计算机基础)和[数据库](#user-content-floppy_disk-大数据)部分是当时找实习的时候参考的[Waking-Up](https://github.com/wolverinn/Waking-Up)中的内容，然后又查了一些资料进行补充和完善。

:poultry_leg: 整个项目的在线版本为[Grayson's Notebook](https://notebook.grayson.top)，在线版本是基于[MrDoc](https://github.com/zmister2016/MrDoc)搭建完成的，大家如果有兴趣的话可以根据[部署指南](http://mrdoc.zmister.com/project-7/doc-1362)搭建自己的知识库，具体的知识库效果可参见[觅道文档效果示例](http://mrdoc.zmister.com/project-20)，然后根据[导入文集（Markdown文件压缩包）](http://mrdoc.zmister.com/project-54/doc-1287)将本项目中最新发布的[releases](https://github.com/graysonwp/DeepinKnowledge/releases)导入到自己搭建的知识库中，便可以拥有自己的知识库了，具体效果如下图。如果大家在搭建或使用的过程中有任何问题，欢迎提出[issues](https://github.com/graysonwp/DeepinKnowledge/issues)来进行讨论。

![image-20210612210922101](media/image-20210612210922101.png)

:strawberry: 推荐大家安装[Octotree](https://notebook.grayson.top/media/attachment/2021/06/octotree-7.0.4_0.crx.zip)和[Markdown Outline for GitHub](https://notebook.grayson.top/media/attachment/2021/06/markdown-outline-for-gitHub-0.1.3_0.crx.zip)两个插件，第一个插件可以将当前项目中的文件以文件夹的格式来组织，这样大家不用每点一个文件，都跳转一下链接；第二个插件可以将Github中Markdown文件以大纲的形式展示，这样切换到文档中的不同目录就比较方便，具体效果如下图。

**Octotree：**

![2021-06-12_21-56-19 (8)](media/2021-06-12_21-56-19%20(8).png)

**Markdown Outline for GitHub：**

![2021-06-12_21-58-35](media/2021-06-12_21-58-35.png)

## 📖 目录

| &nbsp;&nbsp;&nbsp;论文写作&nbsp;&nbsp;&nbsp; |        &nbsp;&nbsp;&nbsp;数据库&nbsp;&nbsp;&nbsp;        | &nbsp;&nbsp;&nbsp;&nbsp;大数据&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Java&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;推荐系统&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;计算机基础&nbsp;&nbsp;&nbsp;&nbsp; |
| :------------------------------------------: | :------------------------------------------------------: | :----------------------------------------------------: | :--------------------------------------: | :----------------------------------------------------------: | :--------------------------------------------------------: |
|   [:cloud:](#user-content-cloud-论文写作)    |       [:computer:](#user-content-computer-数据库)        |   [:floppy_disk:](#user-content-floppy_disk-大数据)    |      [:art:](#user-content-art-java)      |           [:wrench:](#user-content-wrench-推荐系统)           |          [:snake:](#user-content-snake-计算机基础)          |
| &nbsp;&nbsp;&nbsp;机器学习&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;算法笔记&nbsp;&nbsp;&nbsp;&nbsp; |                                                        |                                          |                                                              |                           &nbsp;                           |
|      [:dog:](#user-content-dog-机器学习)      |          [:whale:](#user-content-whale-算法笔记)          |                                                        |                                          |                                                              |                                                            |

### :cloud: 论文写作

| Project  | Article                                                      |      |
| -------- | ------------------------------------------------------------ | ---- |
| 论文写作 | [1、免费下载SCI文献的方法](/论文写作/1、免费下载SCI文献的方法.md) |      |
| 论文写作 | [2、硕士论文的书写方法](/论文写作/2、硕士论文的书写方法.md)  |      |

### :computer: 数据库

| Project | SubProject | Article                                                 |
| ------- | ---------- | ------------------------------------------------------- |
| 数据库  | 数据库基础 | [1.1 事务的概念和特性](/数据库/1.1%20事务的概念和特性.md) |
| 数据库  | 数据库基础 | [1.2 锁](/数据库/1.2%20锁.md)                             |
| 数据库  | 数据库基础 | [1.3 索引](/数据库/1.3%20索引.md)                         |

### :floppy_disk: 大数据

| Project | SubProject     | Article                                                      |
| ------- | -------------- | ------------------------------------------------------------ |
| 大数据  | Hadoop         | [1.1 HDFS读写流程](/大数据/1.1%20HDFS读写流程.md)            |
| 大数据  | MapReduce      | [2.1 MapReduce执行原理](/大数据/2.1%20MapReduce执行原理.md)  |
| 大数据  | MapReduce      | [2.2 MapReduce优化方法](/大数据/2.2%20MapReduce优化方法.md)  |
| 大数据  | Hive(Inceptor) | [3.1 托管表和外表的区别](/大数据/3.1%20托管表和外表的区别.md) |
| 大数据  | Hive(Inceptor) | [3.2 分区表和桶表的区别](/大数据/3.2%20分区表和桶表的区别.md) |
| 大数据  | Hive(Inceptor) | [3.3 分区表和分桶表相关语句](/大数据/3.3%20分区表和分桶表相关语句.md) |
| 大数据  | Hive(Inceptor) | [3.4 导入数据相关语句](/大数据/3.4%20导入数据相关语句.md)    |
| 大数据  | Hive(Inceptor) | [3.5 常见查询语句的区别](/大数据/3.5%20常见查询语句的区别.md) |
| 大数据  | Hive(Inceptor) | [3.6 内存表(Holodesk)特点](/大数据/3.6%20内存表(Holodesk)特点.md) |
| 大数据  | Hive(Inceptor) | [3.7 ORC 事务表](/大数据/3.7%20ORC%20事务表.md)              |
| 大数据  | Hive(Inceptor) | [3.8 merge into 的用法](/大数据/3.8%20merge%20into%20的用法.md) |
| 大数据  | Hive(Inceptor) | [3.10 各种表类型及其区别](/大数据/3.10%20各种表类型及其区别.md) |
| 大数据  | Hive(Inceptor) | [3.11 Inceptor 的优化方法](/大数据/3.11%20Inceptor%20的优化方法.md) |
| 大数据  | Hive(Inceptor) | [3.12 SQL 分类及优化策略](/大数据/3.12%20SQL%20分类及优化策略.md) |
| 大数据  | Hive(Inceptor) | [3.13 ORC 事务表与 Hyperbase 表的区别](/大数据/3.13%20ORC%20事务表与%20Hyperbase%20表的区别.md) |
| 大数据  | Hive(Inceptor) | [3.14 Hive 优化方法](/大数据/3.14%20Hive%20优化方法.md)      |
| 大数据  | Hive(Inceptor) | [3.15 Hive SQL 执行原理](/大数据/3.15%20Hive%20SQL%20执行原理.md) |
| 大数据  | Hive(Inceptor) | [3.16 Sort By、Order By、Distribute By 和 Cluster By 的区别](/大数据/3.16%20Sort%20By、Order%20By、Distribute%20By%20和%20Cluster%20By%20的区别.md) |
| 大数据  | Hive(Inceptor) | [3.17 Hive 中 Join 的种类及其各自的优缺点](/大数据/3.17%20Hive%20中%20Join%20的种类及其各自的优缺点.md) |
| 大数据  | Hive(Inceptor) | [3.18 Join 语句](/大数据/3.18%20Join%20语句.md)              |
| 大数据  | Hive(Inceptor) | [3.19 行转列和列转行](/大数据/3.19%20行转列和列转行.md)      |
| 大数据  | 数据仓库       | [4.1 什么是拉链表](/大数据/4.1%20什么是拉链表.md)            |
| 大数据  | 数据仓库       | [4.2 数据仓库一般分为哪几层](/大数据/4.2%20数据仓库一般分为哪几层.md) |
| 大数据  | HBase          | [5.1 HBase 的特点](/大数据/5.1%20HBase%20的特点.md)          |
| 大数据  | HBase          | [5.2 HBase 和 Hive 的区别](/大数据/5.2%20HBase%20和%20Hive%20的区别.md) |
| 大数据  | HBase          | [5.3 HBase 的 RowKey 设计原则](/大数据/5.3%20HBase%20的%20RowKey%20设计原则.md) |
| 大数据  | HBase          | [5.4 HBase 中 scan 和 get 的功能以及实现的异同](/大数据/5.1%20HBase%20的特点.md) |
| 大数据  | HBase          | [5.5 HBase 的物理结构](/大数据/5.5%20HBase%20的物理结构.md)  |
| 大数据  | HBase          | [5.6 HBase 宕机如何处理](/大数据/5.6%20HBase%20宕机如何处理.md) |
| 大数据  | HBase          | [5.7 HBase 读写流程](/大数据/5.7%20HBase%20读写流程.md)      |
| 大数据  | HBase          | [5.8 HBase 为何要预分区](/大数据/5.8%20HBase%20为何要预分区.md) |
| 大数据  | Kafka          | [6.1 Kafka 的幂等性及其重要性](/大数据/6.1%20Kafka%20的幂等性及其重要性.md) |
| 大数据  | Kafka          | [6.2 Kafka 事务](/大数据/6.2%20Kafka%20事务.md)              |
| 大数据  | Kafka          | [6.3 Kafka 速度快的原因](/大数据/6.3%20Kafka%20速度快的原因.md) |
| 大数据  | Kafka          | [6.4 ISR 机制](/大数据/6.4%20ISR%20机制.md)                  |
| 大数据  | Kafka          | [6.5 ACK 参数有哪几种，分别代表什么含义](/大数据/6.5%20ACK%20参数有哪几种，分别代表什么含义.md) |
| 大数据  | Kafka          | [6.6 Kafka 保证数据不丢失的方法](/大数据/6.6%20Kafka%20保证数据不丢失的方法.md) |
| 大数据  | Kafka          | [6.7 解决重试乱序的方法](/大数据/6.7%20解决重试乱序的方法.md) |
| 大数据  | Spark          | [7.1 Spark与MapReduce的区别](/大数据/7.1%20Spark与MapReduce的区别.md) |
| 大数据  | Spark          | [7.2 Spark性能优化](/大数据/7.2%20Spark性能优化.md)          |
| 大数据  | Spark          | [7.3 Spark中master、worker、executor和driver的关系](/大数据/7.3%20Spark中master、worker、executor和driver的关系.md) |
| 大数据  | Spark          | [7.4 Spark作业运行流程](/大数据/7.4%20Spark作业运行流程.md)  |
| 大数据  | Flink          | [8.1 Flink 作业提交架构流程](/大数据/8.1%20Flink%20作业提交架构流程.md) |
| 大数据  | Flink          | [8.2 DataStream 基本转换](/大数据/8.2%20DataStream%20基本转换.md) |
| 大数据  | Flink          | [8.3 Flink 的时间语义有哪些](/大数据/8.3%20Flink%20的时间语义有哪些.md) |
| 大数据  | Flink          | [8.4 Flink 自带的 Windows 有哪些](/大数据/8.4%20Flink%20自带的%20Windows%20有哪些.md) |
| 大数据  | Flink          | [8.5 什么是 Watermark](/大数据/8.5%20什么是%20Watermark.md)  |
| 大数据  | Flink          | [8.6 设置 Watermark 有哪几种方式](/大数据/8.6%20设置%20Watermark%20有哪几种方式.md) |
| 大数据  | Flink          | [8.7 Flink 如何处理延迟的数据](/大数据/8.7%20Flink%20如何处理延迟的数据.md) |
| 大数据  | Flink          | [8.8 Flink 中的 State 有哪几种](/大数据/8.8%20Flink%20中的%20State%20有哪几种.md) |
| 大数据  | Flink          | [8.9 Flink 的状态后端存储有哪几种](/大数据/8.9%20Flink%20的状态后端存储有哪几种.md) |
| 大数据  | Flink          | [8.10 Savepoint 与 Checkpoint 的区别](/大数据/8.10%20Savepoint%20与%20Checkpoint%20的区别.md) |
| 大数据  | Flink          | [8.11 Checkpoint 流程](/大数据/8.11%20Checkpoint%20流程.md)  |
| 大数据  | Flink          | [8.12 基于 RocksDB 的增量 Checkpoint 实现原理](/大数据/8.12%20基于%20RocksDB%20的增量%20Checkpoint%20实现原理.md) |
| 大数据  | Flink          | [8.13 Flink CEP 的优缺点](/大数据/8.13%20Flink%20CEP%20的优缺点.md) |
| 大数据  | Flink          | [8.14 Flink1.5 之前如何解决反压问题](/大数据/8.14%20Flink1.5%20之前如何解决反压问题.md) |
| 大数据  | Flink          | [8.15 TaskManager 之间如何进行网络传输](/大数据/8.15%20TaskManager%20之间如何进行网络传输md) |
| 大数据  | Flink          | [8.16 NetworkBufferPool、InputGate、和 ResultPartition 的内存是如何申请的](/大数据/8.16%20NetworkBufferPool、InputGate、和%20ResultPartition%20的内存是如何申请的.md) |
| 大数据  | Flink          | [8.17 Flink 的三种动态反馈的具体细节](/大数据/8.17%20Flink%20的三种动态反馈的具体细节.md) |
| 大数据  | Flink          | [8.18 Flink1.5 之前反压机制存在的问题](/大数据/8.18%20Flink1.5%20之前反压机制存在的问题.md) |
| 大数据  | Flink          | [8.19 基于 Credit 的反压机制原理](/大数据/8.19%20基于%20Credit%20的反压机制原理.md) |
| 大数据  | Flink          | [8.20 Flink 如何定位产生反压的位置](/大数据/8.20%20Flink%20如何定位产生反压的位置.md) |
| 大数据  | Flink          | [8.21 定位到反压来源后，该如何处理](/大数据/8.21%20定位到反压来源后，该如何处理.md) |
| 大数据  | Flink          | [8.22 什么是 Slot](/大数据/8.22%20什么是%20Slot.md)          |
| 大数据  | Flink          | [8.23 Slot 和 Parallelism 的关系](/大数据/8.23%20Slot%20和%20Parallelism%20的关系.md) |
| 大数据  | Flink          | [8.24 Flink Checkpoint 的过程](/大数据/8.24%20Flink%20Checkpoint%20的过程.md) |
| 大数据  | Flink          | [8.25 什么是 barrier 对齐](/大数据/8.25%20什么是%20barrier%20对齐.md) |
| 大数据  | Flink          | [8.26 2PC 一致性协议](/大数据/8.26%202PC%20一致性协议.md)    |
| 大数据  | Flink          | [8.27 Flink 中 TwoPhaseCommitSinkFunction 执行流程](/大数据/8.27%20Flink%20中%20TwoPhaseCommitSinkFunction%20执行流程.md) |
| 大数据  | Flink          | [8.28 FlinkKafkaProducer011 的实现原理](/大数据/8.28%20FlinkKafkaProducer011%20的实现原理.md) |
| 大数据  | Flink          | [8.29 Flink 如何保证 Exactly Once](/大数据/8.29%20Flink%20如何保证%20Exactly%20Once.md) |
| 大数据  | Flink          | [8.30 LocalKeyBy 的实现原理](/大数据/8.30%20LocalKeyBy%20的实现原理.md) |
| 大数据  | Flink          | [8.31 DataStream的物理分区策略](/大数据/8.31%20DataStream的物理分区策略.md) |
| 大数据  | Flink          | [8.32 Flink的重启策略](/大数据/8.32%20Flink的重启策略.md)    |

### :art: Java

| Project | SubProject | Article                                                      |
| ------- | ---------- | ------------------------------------------------------------ |
| Java    | Java基础   | [1.1 Java数据类型有哪几种](/Java/1.1%20Java数据类型有哪几种.md) |
| Java    | Java基础   | [1.2 ArrayList与LinkedList的区别？](/Java/1.2%20ArrayList与LinkedList的区别？.md) |
| Java    | Java基础   | [1.3 HashMap、HashTable、ConcurrentHashMap各自的技术原理和使用场景是什么](/Java/1.3%20HashMap、HashTable、ConcurrentHashMap各自的技术原理和使用场景是什么.md) |
| Java    | Java基础   | [1.4 HashMap和HashTable的区别](/Java/1.4%20HashMap和HashTable的区别.md) |
| Java    | Java基础   | [1.5 String、StringBuffer和StringBuilder的区别](/Java/1.5%20String、StringBuffer和StringBuilder的区别.md) |
| Java    | Java多线程 | [2.1 Runnable和 Thread的区别？](/Java/2.1%20Runnable和%20Thread的区别？.md) |
| Java    | Java多线程 | [2.2 Thread类中的start()和run()方法有什么区别？](/Java/2.2%20Thread类中的start()和run()方法有什么区别？.md) |
| Java    | Java多线程 | [2.3 Java多线程中调用wait()和sleep()方法有什么不同？](/Java/2.3%20Java多线程中调用wait()和sleep()方法有什么不同？.md) |
| Java    | Java多线程 | [2.4 ReentrantLock和Synchronized的区别？](/Java/2.4%20ReentrantLock和Synchronized的区别？.md) |
| Java    | Java多线程 | [2.5 volatile适用于高并发的什么场景？](/Java/2.5%20volatile适用于高并发的什么场景？.md) |
| Java    | Java多线程 | [2.6 多线程join方法用于什么场景？](/Java/2.6%20多线程join方法用于什么场景？.md) |
| Java    | Java多线程 | [2.7 多个线程间如何共享数据？](/Java/2.7%20多个线程间如何共享数据？.md) |
| Java    | Java多线程 | [2.8 线程的状态有哪些，各自的使用场景是什么？](/Java/2.8%20线程的状态有哪些，各自的使用场景是什么？.md) |
| Java    | Java多线程 | [2.9 Java自带哪几种线程池](/Java/2.9%20Java自带哪几种线程池.md) |
| Java    | Java多线程 | [2.10 Java内存模型](/Java/2.10%20Java内存模型.md)            |
| Java    | Java多线程 | [2.11 并发编程的三大特性](/Java/2.11%20并发编程的三大特性.md) |
| Java    | Java多线程 | [2.12 指令重排](/Java/2.12%20指令重排.md)                    |
| Java    | Java多线程 | [2.13 Volatile原理](/Java/2.13%20Volatile原理.md)            |
| Java    | Java多线程 | [2.14 CAS原理](/Java/2.14%20CAS原理.md)                      |
| Java    | Java多线程 | [2.15 Java的4种引用级别](/Java/2.15%20Java的4种引用级别.md)  |
| Java    | Java多线程 | [2.16 Java中的锁](/Java/2.16%20Java中的锁.md)                |
| Java    | JVM        | [3.1 运行时数据区域](/Java/3.1%20运行时数据区域.md)          |
| Java    | JVM        | [3.2 判断对象是否存活的方法](/Java/3.2%20判断对象是否存活的方法.md) |
| Java    | JVM        | [3.3 垃圾收集算法](/Java/3.3%20垃圾收集算法.md)              |
| Java    | JVM        | [3.4 类的生命周期和加载过程](/Java/3.4%20类的生命周期和加载过程.md) |
| Java    | JVM        | [3.5 类加载时机](/Java/3.5%20类加载时机.md)                  |
| Java    | JVM        | [3.6 类加载器的种类](/Java/3.6%20类加载器的种类.md)          |
| Java    | JVM        | [3.7 类加载机制的特点](/Java/3.7%20类加载机制的特点.md)      |
| Java    | JVM        | [3.8 JVM内存结构](/Java/3.8%20JVM内存结构.md)                |
| Java    | JVM        | [3.9 常见的垃圾收集算法有哪些](/Java/3.9%20常见的垃圾收集算法有哪些.md) |
| Java    | JVM        | [3.10 指针碰撞和空闲列表](/Java/3.10%20指针碰撞和空闲列表.md) |
| Java    | JVM        | [3.11 常见的垃圾收集器有哪些](/Java/3.11%20常见的垃圾收集器有哪些.md) |
| Java    | JVM        | [3.12 内存溢出与内存泄漏的区别](/Java/3.12%20内存溢出与内存泄漏的区别.md) |
| Java    | JVM        | [3.13 常用的JVM启动参数有哪些](/Java/3.13%20常用的JVM启动参数有哪些.md) |

### :wrench: 推荐系统

| Project  | SubProject | Article                                         |
| -------- | ---------- | ----------------------------------------------- |
| 推荐系统 |            | [1、推荐算法分类](/推荐系统/1、推荐算法分类.md) |

### :snake: 计算机基础

| Project    | SubProject | ThirdSubProject     | Article                                                      |
| ---------- | ---------- | ------------------- | ------------------------------------------------------------ |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.1 三次握手 ](/计算机基础/1.1.1%20三次握手.md)             |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.2 四次挥手 ](/计算机基础/1.1.2%20四次挥手.md)             |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.3 流量控制 ](/计算机基础/1.1.3%20流量控制.md)             |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.5 拥塞控制 ](/计算机基础/1.1.5%20拥塞控制.md)             |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.6 TCP和UDP的区别 ](/计算机基础/1.1.6%20TCP和UDP的区别.md) |
| 计算机基础 | 计算机网络 | 传输层：TCP和UDP    | [1.1.7 TCP如何保证传输的可靠性 ](/计算机基础/1.1.7%20TCP如何保证传输的可靠性.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.1 HTTP和HTTPS的区别 ](/计算机基础/1.2.1%20HTTP和HTTPS的区别.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.2 GET和POST的区别 ](/计算机基础/1.2.2%20GET和POST的区别.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.3 Session与Cookie的区别 ](/计算机基础/1.2.3%20Session与Cookie的区别.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.4 从输入网址到获得页面的过程（越详细越好） ](/计算机基础/1.2.4%20从输入网址到获得页面的过程（越详细越好）.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.5 HTTP请求有哪些常见的状态码 ](/计算机基础/1.2.5%20HTTP请求有哪些常见的状态码.md) |
| 计算机基础 | 计算机网络 | 应用层：HTTP和HTTPS | [1.2.6 什么是RIP，算法是什么 ](/计算机基础/1.2.6%20什么是RIP，算法是什么.md) |
| 计算机基础 | 计算机网络 |                     | [1.3 计算其网络体系结构 ](/计算机基础/1.3%20计算其网络体系结构.md) |
| 计算机基础 | 计算机网络 | 网络层协议          | [1.4.1 IP地址的分类 ](/计算机基础/1.4.1%20IP地址的分类.md)     |
| 计算机基础 | 计算机网络 | 网络层协议          | [1.4.2 划分子网 ](/计算机基础/1.4.2%20划分子网.md)             |
| 计算机基础 | 计算机网络 | 网络层协议          | [1.4.3 什么是ARP协议 ](/计算机基础/1.4.3%20什么是ARP协议.md)   |
| 计算机基础 | 计算机网络 | 网络层协议          | [1.4.4 NAT协议 ](/计算机基础/1.4.4%20NAT协议.md)               |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.1 进程和线程的区别 ](/计算机基础/2.1.1%20进程和线程的区别.md) |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.2 进程间通信方式 ](/计算机基础/2.1.2%20进程间通信方式.md) |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.3 进程同步问题 ](/计算机基础/2.1.3%20进程同步问题.md)     |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.4 进程有哪几种状态 ](/计算机基础/2.1.4%20进程有哪几种状态.md) |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.5 进程调度策略 ](/计算机基础/2.1.5%20进程调度策略.md)     |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.6 僵尸进程和孤儿进程 ](/计算机基础/2.1.6%20僵尸进程和孤儿进程.md) |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.7 线程同步 ](/计算机基础/2.1.7%20线程同步.md)             |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.8 协程 ](/计算机基础/2.1.8%20协程.md)                     |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.9 异常控制流 ](/计算机基础/2.1.9%20异常控制流.md)         |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.10 IO多路复用 ](/计算机基础/2.1.10%20IO多路复用.md)       |
| 计算机基础 | 操作系统   | 进程和线程          | [2.1.11 用户态和内核态 ](/计算机基础/2.1.11%20用户态和内核态.md) |
| 计算机基础 | 操作系统   |                     | [2.2 死锁 ](/计算机基础/2.2%20死锁.md)                         |
| 计算机基础 | 操作系统   | 内存管理            | [2.3.1 分页和分段 ](/计算机基础/2.3.1%20分页和分段.md)         |
| 计算机基础 | 操作系统   | 内存管理            | [2.3.2 虚拟内存 ](/计算机基础/2.3.2%20虚拟内存.md)             |
| 计算机基础 | 操作系统   | 内存管理            | [2.3.3 页面置换算法 ](/计算机基础/2.3.3%20页面置换算法.md)     |
| 计算机基础 | 操作系统   | 内存管理            | [2.3.4 局部性原理 ](/计算机基础/2.3.4%20局部性原理.md)         |
| 计算机基础 | 操作系统   | 内存管理            | [2.3.5 缓冲区溢出 ](/计算机基础/2.3.5%20缓冲区溢出.md)         |
| 计算机基础 | 操作系统   |                     | [2.4 磁盘调度](/计算机基础/2.4%20磁盘调度.md)                  |

### :dog: 机器学习

| Project  | SubProject    | ThirdSubProject | Article                                                      |
| -------- | ------------- | --------------- | ------------------------------------------------------------ |
| 机器学习 |               |                 | [1.1 机器学习方式 ](/机器学习/1.1%20机器学习方式.md)           |
| 机器学习 | 基础知识      | 模型评估        | [1.2.1 错误率与精度 ](/机器学习/1.2.1%20错误率与精度.md)       |
| 机器学习 | 基础知识      | 模型评估        | [1.2.2 查准率与查全率 ](/机器学习/1.2.2%20查准率与查全率.md)   |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.1 决策树的基本原理](/机器学习/2.1.1%20决策树的基本原理.md) |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.2 决策树的三要素](/机器学习/2.1.2%20决策树的三要素.md)    |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.3 决策树算法的优缺点](/机器学习/2.1.3%20决策树算法的优缺点.md) |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.4 熵和信息增益的区别](/机器学习/2.1.4%20熵和信息增益的区别.md) |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.5 剪枝处理的作用及策略](/机器学习/2.1.5%20剪枝处理的作用及策略.md) |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.6 决策树算法-id3](/机器学习/2.1.6%20决策树算法-id3.md)    |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.7 决策树算法-c4.5](/机器学习/2.1.7%20决策树算法-c4.5.md)  |
| 机器学习 | 分类-基本算法 | 决策树          | [2.1.8 决策树算法-cart](/机器学习/2.1.8%20决策树算法-cart.md)  |
| 机器学习 | 分类-组合算法 |                 | [3.1 集成学习概述](/机器学习/3.1%20集成学习概述.md)            |
| 机器学习 | 分类-组合算法 |                 | [3.2 个体学习器](/机器学习/3.2%20个体学习器.md)                |
| 机器学习 | 分类-组合算法 |                 | [3.3 结合策略](/机器学习/3.3%20结合策略.md)                    |
| 机器学习 | 分类-组合算法 |                 | [3.4 Bagging和Boosting的联系与区别](/机器学习/3.4%20Bagging和Boosting的联系与区别.md) |
| 机器学习 | 分类-组合算法 | Bagging         | [3.5.1 随机森林原理](/机器学习/3.5.1%20随机森林原理.md)        |
| 机器学习 | 分类-组合算法 | Boosting        | [3.6.1 AdaBoost原理](/机器学习/3.6.1%20AdaBoost原理.md)        |

### :whale: 算法笔记

| Project  | SubProject | ThirdSubProject | Article                                                      |
| -------- | ---------- | --------------- | ------------------------------------------------------------ |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.1 斐波那契数列 ](/算法笔记/1.1.1%20斐波那契数列.md)       |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.2 凑零钱问题 ](/算法笔记/1.1.2%20凑零钱问题.md)           |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.3 贪心算法 ](/算法笔记/1.1.3%20贪心算法.md)               |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.4 最长递增子序列 ](/算法笔记/1.1.4%20最长递增子序列.md)   |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.5 编辑距离 ](/算法笔记/1.1.5%20编辑距离.md)               |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.6 高楼扔鸡蛋 ](/算法笔记/1.1.6%20高楼扔鸡蛋.md)           |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.7 最长回文子序列 ](/算法笔记/1.1.7%20最长回文子序列.md)   |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.8 最大子序和 ](/算法笔记/1.1.8%20最大子序和.md)           |
| 算法笔记 | 算法框架   | 动态规划        | [1.1.9 买卖股票 ](/算法笔记/1.1.9%20买卖股票.md)               |
| 算法笔记 | 算法框架   |                 | [1.2 二分查找 ](/算法笔记/1.2%20二分查找.md)                   |
| 算法笔记 | 算法框架   | 数据结构        | [1.3.1 反转链表 ](/算法笔记/1.3.1%20反转链表.md)               |
| 算法笔记 | 算法框架   | 数据结构        | [1.3.2 数组中的第k个最大元素 ](/算法笔记/1.3.2%20数组中的第k个最大元素.md) |
| 算法笔记 | 算法框架   |                 | [1.4 排序算法 ](/算法笔记/1.4%20排序算法.md)                   |
| 算法笔记 | 高频面试题 |                 | [2.1 快速模幂算法 ](/算法笔记/2.1%20快速模幂算法.md)           |
| 算法笔记 | 高频面试题 |                 | [2.2 如何运用二分查找算法 ](/算法笔记/2.2%20如何运用二分查找算法.md) |
| 算法笔记 | 高频面试题 |                 | [2.3 如何高效解决接雨水问题 ](/算法笔记/2.3%20如何高效解决接雨水问题.md) |
| 算法笔记 | 高频面试题 |                 | [2.4 删除有序数组中的重复项](/算法笔记/2.4%20删除有序数组中的重复项.md) |
| 算法笔记 | 高频面试题 |                 | [2.5 如何寻找最⻓回⽂⼦串 ](/算法笔记/2.5%20如何寻找最⻓回⽂⼦串.md) |
| 算法笔记 | 高频面试题 |                 | [2.6 如何判定括号的合法性 ](/算法笔记/2.6%20如何判定括号的合法性.md) |
| 算法笔记 | 高频面试题 |                 | [2.7 如何寻找消失的元素 ](/算法笔记/2.7%20如何寻找消失的元素.md) |
| 算法笔记 | 高频面试题 |                 | [2.8 如何寻找缺失和重复的元素 ](/算法笔记/2.8%20如何寻找缺失和重复的元素.md) |
| 算法笔记 | 高频面试题 |                 | [2.9 如何高效判断回文链表 ](/算法笔记/2.9%20如何高效判断回文链表.md) |
| 算法笔记 | 高频面试题 |                 | [2.10 随机算法之水塘抽样算法 ](/算法笔记/2.10%20随机算法之水塘抽样算法.md) |
| 算法笔记 | 日常练习   |                 | [3.1 搜索二维矩阵 ](/算法笔记/3.1%20搜索二维矩阵.md)         |
| 算法笔记 | 日常练习   |                 | [3.2 环形链表 II ](/算法笔记/3.2%20环形链表%20II.md)           |
| 算法笔记 | 日常练习   |                 | [3.3 在二叉树中分配硬币 ](/算法笔记/3.3%20在二叉树中分配硬币.md) |
| 算法笔记 | 日常练习   |                 | [3.4 构造 K 个回文字符串 ](/算法笔记/3.4%20构造%20K%20个回文字符串.md) |
| 算法笔记 | 日常练习   |                 | [3.5 将有序数组转换为二叉搜索树 ](/算法笔记/3.5%20将有序数组转换为二叉搜索树.md) |
| 算法笔记 | 日常练习   |                 | [3.6 找出数组游戏的赢家 ](/算法笔记/3.6%20找出数组游戏的赢家.md) |
| 算法笔记 | 日常练习   |                 | [3.7 最少侧跳次数 ](/算法笔记/3.7%20最少侧跳次数.md)         |
| 算法笔记 | 日常练习   |                 | [3.8 最大数 ](/算法笔记/3.8%20最大数.md)                     |
| 算法笔记 | 日常练习   |                 | [3.9 实现 Trie (前缀树) ](/算法笔记/3.9%20实现%20Trie%20(前缀树).md) |
| 算法笔记 | 日常练习   |                 | [3.10 无重复字符的最长子串 ](/算法笔记/3.10%20无重复字符的最长子串.md) |
| 算法笔记 | 日常练习   |                 | [3.11 LRU 缓存机制 ](/算法笔记/3.11%20LRU%20缓存机制.md)     |
| 算法笔记 | 日常练习   |                 | [3.12 K 个一组翻转链表 ](/算法笔记/3.12%20K%20个一组翻转链表.md) |
| 算法笔记 | 日常练习   |                 | [3.13 两数之和 ](/算法笔记/3.13%20两数之和.md)               |
| 算法笔记 | 日常练习   |                 | [3.14 相交链表 ](/算法笔记/3.14%20相交链表.md)               |
| 算法笔记 | 日常练习   |                 | [3.15 三数之和 ](/算法笔记/3.15%20三数之和.md)               |
| 算法笔记 | 剑指Offer  |                 | [4.1 二维数组中的查找 ](/算法笔记/4.1%20二维数组中的查找.md) |
| 算法笔记 | 剑指Offer  |                 | [4.2 重建二叉树 ](/算法笔记/4.2%20重建二叉树.md)             |

## ❗️ 勘误

- 如果在文章中发现了问题，欢迎提交 PR 或者 issue，欢迎大神们多多指点🙏🙏🙏

## ♥️ Thanks

感谢Star！

[![Stargazers over time](https://starchart.cc/graysonwp/DeepinKnowledge.svg)](https://starchart.cc/graysonwp/DeepinKnowledge)

## ©️ 转载

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />本<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" rel="dct:type">作品</span>由 <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/graysonwp" property="cc:attributionName" rel="cc:attributionURL">Grayson</a> 创作，采用<a rel="license" href="http://creativecommons.org/licenses/by/4.0">知识共享署名 4.0 国际许可协议</a>进行许可。
>>>>>>> parent of c906903 (Update README.md)
